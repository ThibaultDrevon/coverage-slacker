import time

from tabulate import tabulate
from slacker import Slacker


class SlackProxy(object):

    def __init__(self, user_token, channel_id):
        """Creates a new SlackProxy.

        :param channel_id: The channel id where the job should post.
        :type channel_id: str
        :param user_token: Token identifying the user posting on the channel.
        :type user_token: str
        """
        self.slacker = Slacker(user_token)
        self.channel_id = channel_id

    def post_message(self, message):
        """Posts a message using current user on the current channel.

        :param message: The message to post.
        :type message: unicode
        """
        self.slacker.chat.post_message(self.channel_id, message, as_user=True)
        time.sleep(0.5)

    def post_table(self, caption, rows, headers):
        """Posts

        :param caption: A little caption to write before the table.
        :type caption: str
        :param rows: Data rows.
        :type rows: iterable[iterable]
        :param headers: Columns headers, in the same order as the data rows.
        :type headers: iterable[str]
        """
        self.post_message(
            '*{caption}*\n```{table}```'.format(
                caption=caption,
                table=tabulate(rows, headers=headers, floatfmt=('', '', 'g', '+g'))
            )
        )

    def post_image(self, caption, image_path):
        self.slacker.files.upload(image_path, title=caption, channels=self.channel_id)
