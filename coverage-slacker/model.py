import os
import time
import datetime


class DataPoint(object):
    """A single sampling of data among a series."""

    def __init__(self, point_value, point_time=datetime.datetime.now()):
        """Creates a new DataPoint.

        :param point_time: The date and time of the sample.
        :type point_time: datetime.datetime
        :param point_value: The sample value.
        :type point_value: float
        """
        self.time = point_time
        self.value = point_value

    @classmethod
    def from_string(cls, string):
        timestamp, value = string.strip().split(',')
        return DataPoint(float(value), datetime.datetime.utcfromtimestamp(float(timestamp)))

    def to_string(self):
        return ','.join((str(time.mktime(self.time.timetuple())), str(self.value)))


class SeriesIndex(object):
    """The key differentiating series between themselves."""

    def __init__(self, userspace, project):
        self.userspace = userspace
        self.project = project
        self.long_name = (userspace + '.' + project).replace('/', '.')
        self._hash = hash(userspace + '/' + project)

    def __hash__(self):
        return self._hash


class CoverageSeries(object):
    """A collection of samples on a particular dimension."""

    def __init__(self, index, points=None):
        """Creates a new Coverage series.

        :param index: The unique index of that series.
        :type index: SeriesIndex
        :param points: The optional initial sequence of data points.
        :type points: iterable[DataPoint]
        """
        self.index = index
        self.points = {point.time: point for point in points} if points else {}

    @classmethod
    def from_file(cls, index, file_path):
        with open(file_path, 'r') as fd:
            return cls(index, [DataPoint.from_string(line) for line in fd])

    def to_file(self, file_path):
        with open(file_path, 'w') as fd:
            fd.writelines([point.to_string() + '\n' for point in self.points_as_list()])

    def insert_point(self, value):
        """Inserts a new point at the right place according to series sorting."""
        point = DataPoint(value)
        self.points[point.time] = point

    def points_as_list(self):
        """Obtains the points in this series ordered by ascending date.

        :return: The points in this series.
        :rtype: list[DataPoint]
        """
        return [self.points[key] for key in self.point_times()]

    def point_times(self):
        """Gets ordered list of keys (sampling times)."""
        return sorted(self.points.keys())

    def get_last_week_point(self):
        last_week_time = datetime.datetime.now() - datetime.timedelta(days=6)
        for key in reversed(self.point_times()):
            if key.date() <= last_week_time.date():
                return self.points[key]
        return DataPoint(0)

    def get_last_point(self):
        return self.points[self.point_times()[-1]] if self.points else DataPoint(0)


class CoverageStore(object):

    def __init__(self, location):
        """Handles the storing of the coverage time series on the file system.

        :param location: The absolute path of the root folder.
        """
        self.root_location = location
        if not os.path.exists(location):
            os.mkdir(location)

    def get_one_series(self, userspace, project):
        index = SeriesIndex(userspace, project)
        return CoverageSeries.from_file(index, self._get_series_location(index))

    def get_series(self, indexes):
        return [self.get_one_series(index.userspace, index.project) for index in indexes]

    def save_one_series(self, series):
        series_location = self._get_series_location(series.index)
        series.to_file(series_location)

    def save_series(self, series_list):
        for series in series_list:
            self.save_one_series(series)

    def _get_series_location(self, index):
        location = os.path.join(self.root_location, index.long_name)
        if not os.path.exists(location):
            open(location, 'w').close()
        return location
