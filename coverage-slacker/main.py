import os
import datetime

import matplotlib
# This setup must be performed before importing seaborn.
matplotlib.use('TkAgg')
import matplotlib.dates as mdates
import seaborn as sns
import pandas

from gitlab_tools import GitlabProxy
from slack_tools import SlackProxy
from model import CoverageStore, SeriesIndex


GITLAB_USER_TOKEN = 'sb567zQBw7ZQyE3C592A'
GITLAB_MOO_URL = 'http://gitlab.office.moo.com'
DE_TEST_CHANNEL = 'C912MNBML'
DE_CHAT_CHANNEL = 'G09EPMDNK'
COVERAGE_ROOT = os.path.join(os.path.dirname(__file__), 'series')
GIT_DE_USERSPACE = 'data-warehouse'
BUMBLESLACK_USER_TOKEN = "xoxb-305778293777-djlcdVsCCs54BOrCCOWeKNXk"
TEMP_FILE = os.path.join(os.path.dirname(__file__), 'temp.png')

INDEXES = [
    SeriesIndex(GIT_DE_USERSPACE, 'reporting'),
    SeriesIndex(GIT_DE_USERSPACE, 'aws/athena'),
    SeriesIndex(GIT_DE_USERSPACE, 'aws/event-poker'),
    SeriesIndex(GIT_DE_USERSPACE, 'cyclic_services'),
]


def update_series(gl, series_list):
    """Fetch the last coverage for each series in the provided list."""
    for series in series_list:
        coverage = gl.get_current_coverage(series.index.userspace, series.index.project)
        series.insert_point(coverage)


def build_coverage_table(store, series_indexes):
    """Creates table rows containing details of coverage for provided series.

    :param gl: gitlab proxy.
    :type gl: GitlabProxy
    :param store: Coverage data store.
    :type store: CoverageStore
    :param series_indexes:
    :type series_indexes: list[SeriesIndex]
    :return:
    """
    rows = []
    for index in series_indexes:
        series = store.get_one_series(index.userspace, index.project)
        last_val = series.get_last_point().value
        prev_val = series.get_last_week_point().value
        rows.append(
            (index.project, prev_val, last_val, last_val - prev_val)
        )
    return rows


def generate_graph_list_series(series_list, graph_path):
    """Generates a graph for the provided series list.

    :param series_list:
    :type series_list: list[CoverageSeries]
    :param graph_path: Output file path of the graph.
    """
    rows = [
        [mdates.date2num(point.time), point.value, series.index.project]
        for series in series_list for point in series.points_as_list()
    ]
    df = pandas.DataFrame(data=rows, columns=['Date', 'Coverage', 'Project'])
    plot = sns.lmplot(x="Date", y="Coverage", hue='Project', data=df)

    # Replace date numbers by nice labels.
    plot.ax.xaxis.set_major_formatter(mdates.DateFormatter('%b %d'))

    # Add annotation to see a bit better the coverage at each date.
    for pt in set([(pt[0], pt[1]) for pt in rows]):
        plot.ax.annotate(str(pt[1]), xy=(pt[0], pt[1]), xytext=(pt[0], pt[1] + 1))

    plot.savefig(graph_path)


def main():
    store = CoverageStore(COVERAGE_ROOT)
    series_list = store.get_series(INDEXES)

    gl = GitlabProxy(GITLAB_USER_TOKEN, GITLAB_MOO_URL)
    update_series(gl, series_list)
    store.save_series(series_list)

    slacker = SlackProxy(BUMBLESLACK_USER_TOKEN, DE_CHAT_CHANNEL)

    rows = build_coverage_table(store, INDEXES)
    slacker.post_table(
        "Good morning DE! Here are the coverages for this week.",
        rows,
        ["Project", "Last Week", "Now", "Variation"]
    )

    try:
        generate_graph_list_series(series_list, TEMP_FILE)
        slacker.post_image('Coverage as of {}'.format(datetime.datetime.now()), TEMP_FILE)
    except Exception as e:
        print(e)

    if os.path.exists(TEMP_FILE):
        os.remove(TEMP_FILE)


if __name__ == '__main__':
    main()
