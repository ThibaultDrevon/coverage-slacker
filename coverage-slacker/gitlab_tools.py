import gitlab


class GitlabProxy(object):
    """Acts as a proxy with the actual gitlab API."""

    def __init__(self, user_token, base_url):
        self.client = gitlab.Gitlab(base_url, private_token=user_token)

    def get_current_coverage(self, userspace, project_name, branch='master'):
        """Get the up-to-date coverage for the specified project and branch.

        :param userspace: The userspace containing the project.
        :param project_name: The full name aof the project.
        :param branch: The branch to check for coverage.
        :return: The coverage.
        :rtype: int
        """
        try:
            project = self.client.projects.get('{}/{}'.format(userspace, project_name))
            pipelines_on_branch = project.pipelines.list(ref=branch, status='success', order_by='id', sort='desc')
            pipeline_id = pipelines_on_branch[0].id
            pipeline = project.pipelines.get(pipeline_id)
            return float(pipeline.attributes['coverage'])
        except Exception as e:
            print(e)
            return 0.
