# coding: utf-8

from setuptools import setup, find_packages

with open('requirements.txt', 'r') as fin:
    requires = [line for line in fin if not line.startswith('-i')]  # Excepted index instructions lines.

setup(
    name="coverage-slacker",
    version='0.4.0',
    description="Provides coverage news about monitored projects.",
    author='Thibault Drevon',
    author_email="thibault@yellowstones.io",
    url='http://www.yellowstones.io',
    keywords=["Slack", "Coverage", "Gitlab"],
    install_requires=requires,
    setup_requires=['pytest-runner'],
    tests_require=['pytest'],
    packages=find_packages(),
    include_package_data=True,
    long_description="""This project posts on Slack to notify about coverage progression on projects.""",
    test_suite='tests',
    entry_points={
        'console_scripts': [
            'coverage-slacker=coverage-slacker.main'
        ]
    }
)
